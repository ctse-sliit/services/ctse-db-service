'use strict';

/*
 * Your config - DB Connections, Logs, Message, Key, etc...
 */
import mongoose from 'mongoose';

export const connectDB = async () => {
    try {
        const dbConnection = await mongoose.connect(process.env.MONGO_URI);
        if (dbConnection) {
            console.log('MongoDB Connected');
        }
    }
    catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

