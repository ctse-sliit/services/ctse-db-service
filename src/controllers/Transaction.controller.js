import Transaction from '../models/Transaction.model';

export const createTransaction = async (req, res) => {
  const { refOrder, refTransaction, totalCost, date } = req.body;
  try {
    const transaction = new Transaction({ refOrder, refTransaction, totalCost, date });
    const savedTransaction = await transaction.save();
    res.status(201).json({ status: 'success', data: savedTransaction });
  } catch (err) {
    res.status(400).json({ status: 'failure', message: err.message });
  }
};
