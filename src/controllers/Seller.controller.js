import Seller from '../models/Seller.model';

export const createSeller = async (req, res) => {
    const { brandName, refAddresses, sellerRef, mobileNumber, referenceItems } = req.body;
    console.log("request body", req.body);
    try {
        const newSeller = new Seller({
            brandName,
            refAddresses,
            sellerRef,
            mobileNumber,
            referenceItems,
        });
        console.log("newSeller", newSeller);
        const sellerSaved = await newSeller.save();
        res.status(200).send({ data: sellerSaved, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getSeller = async (req, res) => {
    const id = req.params.id;
    try {
        const seller = await Seller.findById(id);
        res.status(200).send({ data: seller, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const addAddressToSeller = async (req, res) => {
    const id = req.params.id;
    const {refAddresses} = req.body;
    console.log("request body", req.body);
    try {
        const seller = await  Seller.findByIdAndUpdate(id, { $push: { refAddresses: refAddresses }}, {new: true});
        res.status(200).send({ data: seller, status: 'success' });
    }
    catch (err) {
        res.status(500).send({ message: err.message, status: 'failure' });
    }
}

export const    updateSeller = async (req, res) => {
    const id = req.params.id;
    const { brandName, refAddresses, sellerRef, mobileNumber, referenceItems } = req.body;
    console.log("request body", req.body);
    try {
        const seller = await Seller.findByIdAndUpdate(id, {brandName, refAddresses, sellerRef, mobileNumber, referenceItems});
        console.log("seller", seller);
        res.status(200).send({ data: seller, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getSellerIdUserRef = async (req, res) => {
    const ref = req.params.ref;
    try {
        const seller = await Seller.findOne({sellerRef: ref});
        res.status(200).send({ data: seller._id, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteSeller = async (req, res) => {
    const id = req.params.id;
    try {
        const seller = await Seller.findByIdAndDelete(id);
        res.status(200).send({ data: seller, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}