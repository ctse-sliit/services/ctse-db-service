import Address from '../models/Address.model';

export const createAddress = async (req, res) => {
    const { firstName,lastName, addressLine1, addressLine2, city, state,postalCode, billingMobile,email } = req.body;
    try {
        const newAddress = new Address({
            firstName,
            lastName,
            addressLine1,
            addressLine2,
            city,
            state,
            postalCode,
            billingMobile,
            email   
        });
        const addressSaved = await newAddress.save();
        res.status(200).send({ data: addressSaved, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getAddress = async (req, res) => {
    const id = req.params.id;
    try {
        const address = await Address.findById(id);
        res.status(200).send({ data: address, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const updateAddress = async (req, res) => {
    const id = req.params.id;
    const { firstName,lastName, addressLine1, addressLine2, city, state,postalCode, billingMobile,email } = req.body;
    try {
        const address = await Address.findByIdAndUpdate(id, {firstName,lastName, addressLine1, addressLine2, city, state, postalCode, billingMobile,email});
        res.status(200).send({ data: address, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteAddress = async (req, res) => {
    const id = req.params.id;
    try {
        const address = await Address.findByIdAndDelete(id);
        res.status(200).send({ data: address, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}