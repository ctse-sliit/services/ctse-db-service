import Buyer from "../models/Buyer.model"

export const createBuyer = async (req, res) => {
    const {userRef} = req.body;
    try {
        const newBuyer = new Buyer({
            userRef,
        });
        const buyerSaved = await newBuyer.save();
        res.status(200).send({ data: buyerSaved, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getBuyer = async (req, res) => {
    const id = req.params.id;
    try {
        const buyer = await Buyer.findById(id);
        res.status(200).send({ data: buyer, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getBuyerIdUsingRef = async (req, res) => {
    const ref = req.params.ref;
    try {
        const buyer = await Buyer.findOne({userRef: ref});
        res.status(200).send({ data: buyer._id, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteBuyer = async (req, res) => {
    const id = req.params.id;
    try {
        const buyer = await Buyer.findByIdAndDelete(id);
        res.status(200).send({ data: buyer, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}