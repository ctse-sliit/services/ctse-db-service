'use strict';


export const getValue = (req, res, next) => {
  try {
    res.status(200).send({
      data: [],
      status: 'success',
    });
  } catch (err) {
    res.status(404).send({
      message: err.message,
      status: 'failure',
    });
  }
};


export const getValueById = (req, res, next) => {
  try {
    res.status(200).send({
      data: [],
      status: 'success',
    });
  } catch (err) {
    res.status(404).send({
      message: err.message,
      status: 'failure',
    });
  }
};


export const addValue = (req, res, next) => {
  try {
    res.status(200).send({
      data: 'Value added',
      status: 'success',
    });
  } catch (err) {
    res.status(404).send({
      message: err.message,
      status: 'failure',
    });
  }
};


export const updateValue = (req, res, next) => {
  try {
    res.status(200).send({
      data: 'Value updated',
      status: 'success',
    });
  } catch (err) {
    res.status(404).send({
      message: err.message,
      status: 'failure',
    });
  }
};


export const deleteValue = (req, res, next) => {
  try {
    res.status(200).send({
      data: 'Value deleted',
      status: 'success',
    });
  } catch (err) {
    res.status(404).send({
      message: err.message,
      status: 'failure',
    });
  }
};
