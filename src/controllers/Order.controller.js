import Order from '../models/Order.model';

export const createOrder = async (req, res) => {
  const {
    refBuyer,
    refSeller,
    refAddress,
    itemList,
    tracking,
    grossPrice,
    deliveryCharge,
    serviceFee,
    bankPaymentCharge,
    totalPrice
  } = req.body;
  const uniqueSellers = [...new Set(refSeller)];
  console.log(uniqueSellers);
  try {
    const newOrder = new Order({
      refBuyer,
      refSeller:uniqueSellers,
      refAddress,
      itemList,
      tracking,
      grossPrice,
      deliveryCharge,
      serviceFee,
      bankPaymentCharge,
      totalPrice
    });
    const orderSaved = await newOrder.save();
    res.status(200).send({ data: orderSaved, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const getOrder = async (req, res) => {
  const id = req.params.id;
  try {
    const order = await Order.findById(id).populate('refBuyer').populate('refAddress').populate('itemList.item');
    res.status(200).send({ data: order, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const updateOrder = async (req, res) => {
  const id = req.params.id;
  const {
    refBuyer,
    refSeller,
    refAddress,
    itemList,
    tracking,
    status,
    grossPrice,
    deliveryCharge,
    serviceFee,
    bankPaymentCharge,
    totalPrice
  } = req.body;
  const uniqueSellers = [...new Set(refSeller)];
  try {
    const order = await Order.findByIdAndUpdate(id, {
      refBuyer,
      refSeller:uniqueSellers,
      refAddress,
      itemList,
      tracking,
      status,
      grossPrice,
      deliveryCharge,
      serviceFee,
      bankPaymentCharge,
      totalPrice
    });
    if (order) {
      res.status(200).send({ data: order, status: 'success' });
    } else {
      res.status(404).send({ message: 'Order not found', status: 'failure' });
    }
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const deleteOrder = async (req, res) => {
  const id = req.params.id;
  try {
    const order = await Order.findByIdAndDelete(id);
    res.status(200).send({ data: order, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const getOrdersByBuyer = async (req, res) => {
  const refBuyer = req.params.refBuyer;
  try {
    const orders = await Order.find({ refBuyer }).populate('refBuyer').populate('refAddress').populate('itemList.item');
    res.status(200).send({ data: orders, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const getOrdersBySeller = async (req, res) => {
  const refSeller = req.params.refSeller;
  try {
    const orders = await Order.find({ refSeller }).populate('refBuyer').populate('refAddress').populate('itemList.item');
    res.status(200).send({ data: orders, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};

export const getOrdersByStatus = async (req, res) => {
  const status = req.params.status;
  try {
    const orders = await Order.find({ status }).populate('refBuyer').populate('refAddress').populate('itemList.item');
    res.status(200).send({ data: orders, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};
export const updateOrderStatus = async (req, res) => {
    const id = req.params.id;
    const { status } = req.body;
    try {
      const order = await Order.findByIdAndUpdate(id, { status });
      res.status(200).send({ data: order, status: 'success' });
    } catch (err) {
      res.status(404).send({ message: err.message, status: 'failure' });
    }
  }

//get all orders
export const getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find({}).populate('refBuyer').populate('refAddress').populate('itemList.item').populate('refSeller');
    res.status(200).send({ data: orders, status: 'success' });
  } catch (err) {
    res.status(404).send({ message: err.message, status: 'failure' });
  }
};
