import Item from '../models/Item.model';

export const createItem = async (req, res) => {
    const { name,refSeller,price,quantity, images, description, keywords, category, bestBy } = req.body;
    try {
        const newItem = new Item({
            name,
            refSeller,
            price,
            quantity,
            images,
            description,
            keywords,
            category,
            bestBy,
        });
        const itemSaved = await newItem.save();
        res.status(200).send({ data: itemSaved, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getItem = async (req, res) => {
    const id = req.params.id;
    try {
        const item = await Item.findById(id);
        res.status(200).send({ data: item, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getAllItem = async (req, res) => {
    try {
        const item = await Item.find({});
        res.status(200).send({ data: item, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const updateItem = async (req, res) => {
    const id = req.params.id;
    const { name,refSeller,price,quantity, images, description, keywords, category, bestBy } = req.body;
    try {
        const item = await Item.findByIdAndUpdate(id, {name,refSeller,price,quantity,images, description, keywords, category, bestBy});
        res.status(200).send({ data: item, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteItem = async (req, res) => {
    const id = req.params.id;
    try {
        const item = await Item.findByIdAndDelete(id);
        res.status(200).send({ data: item, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const reduceQty = async (req, res) => {
    const {itemList} = req.body;
    console.log(itemList);
    try {
      const updatedItems = [];
      for (const itemData of itemList) {
        const { item, quantity } = itemData;
        const updatedItem = await Item.findByIdAndUpdate(item, { $inc: { quantity: -quantity } }, { new: true });
        console.log(`Updated item ${item} quantity to ${updatedItem.quantity}`);
        updatedItems.push(updatedItem);
      }
      res.status(200).send({ status: 'success', updatedItems });
    } catch (err) {
      res.status(404).send({ message: err.message, status: 'failure' });
    }
    
}
