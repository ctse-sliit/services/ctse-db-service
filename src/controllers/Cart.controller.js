import Cart from "../models/Cart.model"

export const createCart = async (req, res) => {
    const { buyerRef, item } = req.body;
    console.log("request body", req.body);
    try {
        const cart = new Cart({buyerRef, item});
        console.log("cart", cart);
        const cartSaved = await cart.save();
        console.log("cartSaved", cartSaved);
        res.status(200).send({ data: cartSaved, status: 'success' });
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getCartByBuyer = async (req,res) => {
    const id = req.params.id;
    try {
        const cart = await Cart.findOne({buyerRef: id}).populate('item.id').populate('buyerRef');
        res.status(200).send({ data: cart, status: 'success' });
    }
    catch(err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const addItemToCart = async (req,res) => {
    const buyerId = req.params.id;
    const { item, quantity } = req.body;
    try {
        const cart = await Cart.findOne({buyerRef: buyerId});
        const itemExists = cart.item.find((product) => product == item);
        if (!itemExists) {
            cart.item.push({itemId: item, quantity: quantity});
            const cartSaved = await cart.save();
            res.status(200).send({ data: cartSaved, status: 'success' });
        }
        else {
            res.status(200).send({ data: itemExists, status: 'already exist' });
        }
    }
    catch(err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const updateQuantityOfItemInCart = async (req,res) => {
    const id = req.params.id;
    const { item, quantity } = req.body;
    try {
        const cart = await Cart.findById(id);
        const itemId = cart.item.find((product) => {
            if (product.itemId == item) {
                return product;
            }
        });
        console.log("itemId", itemId);
        if (itemId) {
            const updatedQuantity = await Cart.updateOne(
                { _id: id, 'item._id': itemId._id }, // Match the cart by ID and the item by its ID
                { $set: { 'item.$[elem].quantity': quantity } }, // Set the quantity of the matched item
                { arrayFilters: [{ 'elem._id': itemId._id }] } // Specify the array filter to match the item by its ID
              );
              console.log("updatedQuantity", updatedQuantity);
            res.status(200).send({ data: updatedQuantity, status: 'success' });
        }
    }
    catch(err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteItemFromCart = async (req,res) => {
    const id = req.params.id;
    const { item } = req.body;
    try {
        const cart = await Cart.findById(id);
        const itemId = cart.item.find((product) => {
            if (product.itemId == item) {
                return product;
            }
        }); // Find the item in the cart
        console.log("itemId", itemId);
        if (itemId) {
            const updatedCart = await Cart.updateOne(
                { _id: id }, // Match the cart by ID
                { $pull: { item: { _id: itemId._id } } }, // Remove the matched item
              );
              console.log("updatedCart", updatedCart);
            res.status(200).send({ data: updatedCart, status: 'success' });
        }
    }
    catch(err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteCart = async (req,res) => {
    const id = req.params.id;
    try {
        const cart = await Cart.findByIdAndDelete(id);
        res.status(200).send({ data: cart, status: 'success' });
    }
    catch(err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const clearCart = async (req,res) => {
    const id = req.params.id;
    try {
        const cart = await Cart.findOne({buyerRef:id});
        cart.item = [];
        const cartSaved = await cart.save();
        res.status(200).send({ data: cartSaved, status: 'success' });
    }
    catch(err) {
        res.status(400).send({ message: err.message, status: 'failure' });
    }
}