import { Router } from 'express';
import * as ctrlItem from '../controllers/Item.controller';

const router = Router();

router.post('/', ctrlItem.createItem);
router.get('/:id', ctrlItem.getItem);
router.put('/:id', ctrlItem.updateItem);
router.delete('/:id', ctrlItem.deleteItem);
router.get('/',ctrlItem.getAllItem);
router.patch('/',ctrlItem.reduceQty)

export default router;
