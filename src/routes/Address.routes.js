import {Router} from 'express';
import * as ctrlAddress from '../controllers/Address.controller';

const router = Router();

router.post('/', ctrlAddress.createAddress);
router.get('/:id', ctrlAddress.getAddress);
router.put('/:id', ctrlAddress.updateAddress);
router.delete('/:id', ctrlAddress.deleteAddress);

export default router;
