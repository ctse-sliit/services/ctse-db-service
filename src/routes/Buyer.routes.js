import {Router} from "express"
import * as ctrlBuyer from "../controllers/Buyer.controller";

const route = Router();

route.post("/", ctrlBuyer.createBuyer);
route.get("/:id", ctrlBuyer.getBuyer);
route.get("/userRef/:ref", ctrlBuyer.getBuyerIdUsingRef);
route.delete("/:id", ctrlBuyer.deleteBuyer);

export default route;