import {Router} from 'express';
import * as ctrlCart from '../controllers/Cart.controller';

const route = Router();

route.post('/', ctrlCart.createCart);
route.get('/:id', ctrlCart.getCartByBuyer);
route.put('/addItem/:id', ctrlCart.addItemToCart);
route.put('/updateQuantity/:id', ctrlCart.updateQuantityOfItemInCart);
route.put('/removeItem/:id', ctrlCart.deleteItemFromCart);
route.delete('/:id', ctrlCart.deleteCart);
route.put('/clear/:id', ctrlCart.clearCart);

export default route;
