import { Router } from 'express';
import * as ctrlOrder from '../controllers/Order.controller';

const router = Router();

router.post('/', ctrlOrder.createOrder);
router.get('/:id', ctrlOrder.getOrder);
router.get('/', ctrlOrder.getAllOrders);
router.put('/:id', ctrlOrder.updateOrder);
router.delete('/:id', ctrlOrder.deleteOrder);
router.get('/buyer/:refBuyer', ctrlOrder.getOrdersByBuyer);
router.get('/seller/:refSeller', ctrlOrder.getOrdersBySeller);
router.get('/status/:status', ctrlOrder.getOrdersByStatus);
router.patch('/:id/status', ctrlOrder.updateOrderStatus);

export default router;
