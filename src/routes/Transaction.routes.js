import {Router} from 'express';
import * as ctrlTransaction from '../controllers/Transaction.controller';

const router = Router();

// Route to create a new transaction
router.post('/', ctrlTransaction.createTransaction);

module.exports = router;
