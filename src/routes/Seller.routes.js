import {Router} from 'express';
import * as ctrlSeller from '../controllers/Seller.controller';

const router = Router();

router.post('/', ctrlSeller.createSeller);
router.get('/:id', ctrlSeller.getSeller);
router.get('/userRef/:ref', ctrlSeller.getSellerIdUserRef);
router.put('/:id', ctrlSeller.updateSeller);
router.put('/addAddress/:id', ctrlSeller.addAddressToSeller);
router.delete('/:id', ctrlSeller.deleteSeller);

export default router;
