'use strict';

import {app, contextPath} from './app';
import chalk from 'chalk';
import 'dotenv/config';

app.set('port', process.env.PORT || 3001);
app.set('message', `Server running in ${contextPath} context path with port`);

const port = app.get('port');
const message = app.get('message');



app.listen(port, () => {
  console.log(chalk.blue(`${message} ${port}`));
});
