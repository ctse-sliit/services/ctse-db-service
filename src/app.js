'use strict';

import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import 'dotenv/config';
import mainRoutes from './routes';
import { connectDB } from './config/config';
import AddressRoutes from './routes/Address.routes';
import SellerRoutes from './routes/Seller.routes';
import ItemRoutes from './routes/Items.routes';
import OrderRoutes from './routes/Order.routes';
import CartRoutes from './routes/Cart.routes';
import BuyerRoutes from './routes/Buyer.routes';
import TransactionRoutes from './routes/Transaction.routes';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  app.use(require('morgan')('dev'));
}

app.use(helmet());
app.use(express.static(__dirname + '/src/public'));

//DB connection
connectDB();

/* Routes */

// This context path unique to this service
const contextPath = "/api/db-service"

app.get(`${contextPath}/health`, (req, res) => {
  const data = {
    uptime: process.uptime(),
    message: 'Ok',
    date: new Date()
  }

  res.status(200).send(data);
});

app.use(contextPath, mainRoutes);
app.use(`${contextPath}/address`, AddressRoutes);
app.use(`${contextPath}/seller`, SellerRoutes);
app.use(`${contextPath}/order`, OrderRoutes);
app.use(`${contextPath}/item`, ItemRoutes);
app.use(`${contextPath}/buyer`, BuyerRoutes);
app.use(`${contextPath}/cart`, CartRoutes);
app.use(`${contextPath}/transaction`, TransactionRoutes);

export {app, contextPath};
