import mongoose from 'mongoose';
import Address from '../models/Address.model';
import Item from '../models/Item.model';
const { Schema } = mongoose;

const sellerSchema = new Schema({
  brandName: {
    type: String,
  },
  refAddresses: [{
    type: Schema.Types.ObjectId,
    ref: 'Address'
  }],
  sellerRef: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String
  },
  referenceItems: [{
    type: Schema.Types.ObjectId,
    ref: 'Item'
  }]
}, { timestamps: true });

const Seller = mongoose.model('Seller', sellerSchema);
module.exports = Seller;

