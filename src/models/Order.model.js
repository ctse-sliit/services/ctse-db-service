import mongoose from 'mongoose';
import Buyer from '../models/Buyer.model';
import Selller from '../models/Seller.model';
import Address from '../models/Address.model';
import item from '../models/Item.model';


const { Schema } = mongoose;

const orderSchema = new Schema({
  refBuyer:{
    type:Schema.Types.ObjectId,
    ref:'Buyer',
    required:true
  },
  refSeller:[{
    type:Schema.Types.ObjectId,
    ref:'Seller',
    required:true
  }],
  refAddress: {
    type: Schema.Types.ObjectId,
    ref: 'Address',
    required: true
  },
  itemList: [{
    item: {
      type: Schema.Types.ObjectId,
      ref: 'Item',
      required: true
    },
    quantity: {
      type: Number,
      required: true
    }
  }],
  status: {
    type: String,
    enum: ['pending','shippedtoCenter','approved','shippedtoCustomer','delivered','recived','cancelled'],
    default: 'pending'
  },
  tracking:{
    sellerToCenter:{type:String},
    centerToBuyer:{type:String}
  },
  grossPrice: {
    type: Number,
    required: true
  },
  deliveryCharge: {
    type: Number,
    required: true
  },
  serviceFee: {
    type: Number,
    required: true
  },
  bankPaymentCharge: {
    type: Number,
    required: true
  },
  totalPrice: {
    type: Number,
    required: true
  }
}, {timestamps: true});

const Order = mongoose.model('Order', orderSchema);
module.exports = Order;
