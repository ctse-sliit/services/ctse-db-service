import mongoose from 'mongoose';
import order from './Order.model';

const transactionSchema = new mongoose.Schema({
  refOrder: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order',
    required: true
  },
  refTransaction: {
    type: String,
    required: true
  },
  totalCost: {
    type: Number,
    required: true
  },
  date: {
    type: Number,
    required: true
  }
});

const Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;
