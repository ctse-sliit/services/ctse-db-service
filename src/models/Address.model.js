import mongoose from 'mongoose';
const { Schema } = mongoose;

const addressSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName:{
    type:String,
    required:true
  },
  addressLine1: {
    type: String,
    required: true
  },
  addressLine2: {
    type: String,
  },
  city: {
    type: String,
    required: true
  },
  state: {
    type: String
  },
  postalCode: {
    type: String,
    required: true
  },
  billingMobile: {
    type: String,
    required: true
  },
  email:{
    type:String,
    required:true
  }
}, { timestamps: true });

const Address = mongoose.model('Address', addressSchema);
module.exports = Address;
