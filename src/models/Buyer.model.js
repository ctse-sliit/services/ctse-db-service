import mongoose from "mongoose";
const {Schema}=mongoose;

const buyerSchema= new Schema({
    userRef:{
        type:String,
    },
}, {timestamps: true});

const Buyer= mongoose.model("Buyer",buyerSchema);
module.exports = Buyer;