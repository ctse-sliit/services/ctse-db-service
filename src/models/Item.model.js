import mongoose from 'mongoose';

const { Schema } = mongoose;

const itemSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  refSeller:{
    type:Schema.Types.ObjectId,
    ref:'Buyer',
    required:true
  },
  price:{
    type:Number,
    required:true
  },
  quantity:{
    type:Number,
    required:true
  },
  images: [{
    type: String
  }],
  description: {
    type: String
  },
  keywords: [{
    type: String
  }],
  category: {
    type: String
  },
  bestBy: {
    type: Date
  }
},{timestamps: true});

const Item = mongoose.model('Item', itemSchema);
module.exports = Item;
