import mongoose from "mongoose";
import { Schema } from "mongoose";

const cartSchema = new Schema({
    buyerRef: {
        type: Schema.Types.ObjectId,
        ref: "Buyer",
        required:true,
    },
    item:[{
        itemId:{
            type:Schema.Types.ObjectId,
            ref:"Item"
        },
        quantity:Number,
    }],

},  {timestamps: true});

const Cart = mongoose.model("Cart", cartSchema);
module.exports = Cart;